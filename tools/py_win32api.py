#!/usr/bin/env python
# -*- coding: utf-8 -*-
import win32api


def ShellExecute(hwnd: int = 0, op: str = '', file: str = '', params: str = '', dir: str = '',
                 bShow: int = 1):
    """
        :comments
    Opens or prints a file.
        :param
    Parameters
    hwnd : PyHANDLE
    The handle of the parent window, or 0 for no parent. This window receives any message boxes an application produces (for example, for error reporting).
    op : string
    The operation to perform. May be "open", "print", or None, which defaults to "open".
    file : string
    The name of the file to open.
    params : string
    The parameters to pass, if the file name contains an executable. Should be None for a document file.
    dir : string
    The initial directory for the application.
    bShow : int
    Specifies whether the application is shown when it is opened. If the lpszFile parameter specifies a document file, this parameter is zero.
    """

    return win32api.ShellExecute(hwnd, op, file, params, dir, bShow)
